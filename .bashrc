#################################################################
#                                                               #
#   .bashrc file                                                #
#                                                               #
#   Executed from the bash when you start shell                 #
#                                                               #
#################################################################

if [[ $- != *i* ]] ; then
    # Shell is non-interactive.  Be done now!
    return
fi


# Put your fun stuff here.
. /usr/libexec/mc/mc.sh

if [ $TERM == xterm ] ; then 
    TERM=xterm-256color
fi

#aliases
alias св='cd'

#aliases for git
alias ga='git add'
alias gst='git status'
alias gb='git branch'
alias gc='git commit --message'
alias gush='git push'
alias gull='git pull'
alias gash='git add . && git commit --message "Update files" && git push'
alias feh='feh -g 800x600'

alias fetch2="neofetch \
--ascii distro \
--block_range 0 15 \
--line_wrap off \
--uptime_shorthand on \
--gpu_shorthand on \
--gtk_shorthand on \
--shell_path off \
--shell_version on \
"
