#!/bin/bash
# ----------------------------------------
# $HOME/dotfiles/scripts/i3_restart.sh
# ----------------------------------------
killall conky
# killall lemonbar

# Redraw wallpaper
$HOME/dotfiles/scripts/wallpaper.sh

# Redraw conky
sleep 3 && conky -c $HOME/dotfiles/scripts/conkyrc

Restart lemonbar
# $HOME/dotfiles/scripts/lemonbar2.sh

i3 restart &>/dev/null
