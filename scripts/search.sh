#/bin/bash
# Searching by DuckDuckGo
# description in https://duckduckgo.com/params

search=`xclip -o | sed 's/ /+/g'`
lang="ru-ru"
URL="https://duckduckgo.com/?q=$search&kl=$lang"
echo $URL 
firefox -new-tab $URL
