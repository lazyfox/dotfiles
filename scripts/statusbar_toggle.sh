#!/bin/bash

# The lemonbar toggle

bar() {
    echo `$HOME/dotfiles/scripts/statusbar.sh`
}
PRN_FILE="/tmp/is_bar.txt"

if [ -e $PRN_FILE ]
then
    rm $PRN_FILE && killall statusbar.sh
else
    touch $PRN_FILE && bar
fi    
