#!/bin/bash
# Redraw the wallpaper or install new if $1 is not empty and exits

# For drawing wallpaper on two monitors, feh must be compiled with
# "xinerama" flag

if [ -f "$1" ]; then
    cp $1 $HOME/dotfiles/wallpaper
fi

feh --bg-scale $HOME/dotfiles/wallpaper
