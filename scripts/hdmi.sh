#!/bin/bash
# ----------------------------------------
# $HOME/dotfiles/scripts/hdmi.sh
# ----------------------------------------

IN="LVDS-1-0"
EXT="HDMI-1-0"
CONF_FILE=$HOME/.asoundrc

if [ -f $CONF_FILE ]
then
    rm $CONF_FILE
fi

# Monitors toggle
if (xrandr | grep "$EXT connected"); then
    xrandr --output $IN --mode 1600x900 --pos 0x0 \
            --output $EXT  --mode 1360x768 --pos 1600x0
    echo "defaults.pcm.card 0" > $CONF_FILE
    echo "defaults.pcm.device 3" >> $CONF_FILE
    echo "defaults.ctl.card 0" >> $CONF_FILE
else
    xrandr --output $IN --auto --output $EXT --off
fi

# Restart
i3 restart &>/dev/null

$HOME/dotfiles/scripts/wallpaper.sh

