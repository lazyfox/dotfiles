#!/bin/sh

# Source colors
source ~/dotfiles/colors/colors.sh

# define geometry
barx=550
bary=20
barw=500
barh=33

cpu_load() {
    LINE=`ps -eo pcpu |grep -vE '^\s*(0.0|%CPU)' | sed ':a;N;$!ba;s/\n/ +/g'`
    bc <<< $LINE
}
cpu_temp() {
    core_temp=`sensors | grep "^Core 0" | gawk '{print $3}' | sed -e 's/^.//;s/.\{4\}$//'`
    echo -n $core_temp
}
cpu_freq() {
    line=`grep "cpu MHz" /proc/cpuinfo | gawk '{print $4}' | sed ':a;N;$!ba;s/\n/ /g'`
    echo $line
}

nvidia_temp() {
    nvidia_temp=`nvidia-smi -q -d TEMPERATURE | sed -n '/GPU Current/p' | gawk '{print $5}'`
    echo -n $nvidia_temp
}

while :; do
    buf="%{c}"
    buf="${buf}$(cpu_load)%"
    buf="${buf}  $(cpu_temp)°C"
    buf="${buf}  $(cpu_freq)"
    # buf="${buf}     $(neofetch --stdout memory)"
    # buf="${buf}     $(date "+%a %d %b %l:%M %p")"
    # buf="${buf}     $(mpc current | cut -c 1-25)"

    echo "$buf"
    # use `nowplaying scroll` to get a scrolling output!
    sleep 2s # The HUD will be updated every second
done | lemonbar -g "${barw}x${barh}+${barx}+${bary}" -B "$background" -F "$foreground" -d
