#!/bin/bash
pkill -x dmenu
dmenu_run \
    -p 'Run: ' \
    -fn 'Terminus2-9' \
    -nb '#170f0d' \
    -nf '#c0b18b' \
    -sb '#af865a' \
# j4-dmenu-desktop \
#     --dmenu="dmenu \
#     -i \
#     -p '>> ' \
#     -fn 'Sans-9' \
#     -nb '#161A1F' \
#     -nf '#D3DAE3' \
#     -sb '#161A1F' \
#     -sf '#5294E2'"
# -fn шрифт
# -p приглашение
# -nf шрифт текста
# -nb цвет панели
# -sb цвет фона графич. элементов
# -sf цвет выделяемых надписей
# -i без учета регистра
