#!/bin/sh
# The script checks out the state of the charge of battery
# based on http://blog.z3bra.org/2014/04/pop-it-up.html

levl=20
batc=`sed 's/%//' /sys/class/power_supply/BAT0/capacity`
popup_cmd="$HOME/dotfiles/scripts/popup.sh"

[ $batc -le $levl ] && $popup_cmd battery level: $batc || exit 0
