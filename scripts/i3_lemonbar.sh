#!/bin/bash
killall lemonbar

#define colors
source "$HOME/dotfiles/colors/colors.sh"

#define fonts
# font="Sans-8"
font_powerline="-xos4-terminesspowerline-medium-r-normal--12-120-72-72-c-60-iso10646-1"
icon_font="FontAwesome-8"
font_japanese="Droid Sans Japanese-7"
# icon_font="-xos4-terminusicons2mono-medium-r-normal--12-120-72-72-m-60-iso8859-1"

# Char glyps for powerline fonts
sep_left=""                        # Powerline separator left
sep_right=""                       # Powerline separator right
sep_l_left=""                      # Powerline light separator left
sep_l_right=""                     # Powerline light sepatator right

#CPU usage: system + user + idle = 100%
cpu_usage () {
    # CPU_USAGE=$(grep 'cpu ' /proc/stat | awk '{usage=($2+$4)*100/($2+$4+$5)} END {print usage "%"}')
    CPU_USAGE=$(grep 'cpu ' /proc/stat | awk '{usage=($2+$4)*100/($2+$4+$5)} END {printf "%.1f%", usage}')
    echo -n "%{F$black}${sep_left}%{F$white}%{B$black} CPU: $CPU_USAGE"
}
# Define the clock
Clock() {
    DATETIME=$(date "+%a %b %d, %T")
    echo -n "%{F$green} ${sep_left}%{F$black}%{B$green} $DATETIME"
}

Battery() {
    BATPERC=$($I3_FOLDER/battery.sh)
    echo -n "%{F$black} ${sep_left}%{F$white}%{B$black} $BATPERC"
}

Volume() {
    VOL=$($I3_FOLDER/volume.sh)
    echo -n "%{F$light_black} ${sep_left}%{F$white}%{B$light_black} $VOL"
}

#MPD
music() {
    CURSONG=$(mpc current)
    if [ -z $CURSONG ]
    then
	echo -n "%{F$light_black}${sep_left}%{B$light_black}"
    else
	echo -n "%{F$light_black} ${sep_left}%{F$white}%{B$light_black} \uf001 $CURSONG "
    fi
}

ws() {
    local line
    output="%{F$black}%{B$green} Tags: "
    while read line; do
	num=$(echo "${line}" | cut -d , -f 2 | cut -c 2-7)
	if [[ "$line" == *'true'* ]]; then
	    output="${output}%{F$green}%{B$light_green}${sep_right}%{F$black} ${num} %{F$light_green}%{B$green}${sep_right}"
	elif [[ "$line" == *'false'* ]]; then
	    output="${output}%{F$black}%{B$green} ${num} "
	fi
    done <<< "$(i3-msg -t get_workspaces | jq -S -M -c -r '.[] | [.focused, .name]')"
    echo -e "${output}%{F-}%{B-}"
}

keyboard() {
    LANG=$(xset -q | gawk 'BEGIN { a[1]="Ru"; a[0]="En"} /LED/ { print a[substr($10,5,1)]; }')
    echo -n "%{F$light_green} \ue0b2%{F$black}%{B$light_green} $LANG"
}

video_temp() {
    NVIDIA=$(nvidia-smi -q -d TEMPERATURE | sed -n '/GPU Current/p' | gawk '{print $5}')
    echo -n "%{F$light_black} ${sep_left}%{F$white}%{B$light_black} NVIDIA: $NVIDIA"
}

mail_check() {
    MAIL=$($I3_FOLDER/mail_checker.py)
    if [ -z $MAIL ]
    then
	echo
    else
	echo -n "%{F$red} ${sep_left}%{F$black}%{B$red} $MAIL%{B$red}"
    fi
}
# Finally, print bar
while true; do
    echo -e "%{l}$(ws)%{F$green}${sep_right} %{r}$(mail_check)$(music)$(cpu_usage)$(video_temp)°C$(Battery)$(Volume)$(Clock)$(keyboard) %{F-}%{B-}"
    sleep 1
# done | lemonbar -p -f "$font_powerline" -f "$icon_font" -f "$font_japanese" -B "$background"
done | lemonbar -g "1600x14" -f "$font_powerline" -f "$icon_font" -f "$font_japanese" -B "$background" -p
