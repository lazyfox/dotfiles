#!/bin/sh
# Popup script
# based on http://blog.z3bra.org/2014/04/pop-it-up.html

# Source colors
source ~/dotfiles/colors/colors.sh

# how long should the popup remain?
duration=3

# define geometry
barx=1456
bary=57
barw=120
barh=33

# font used
# bar_font='-xos4-terminesspowerline-medium-r-normal--12-120-72-72-c-60-iso10646-1'
bar_font="Noto Sans-10"

# Create the popup and make it live for 3 seconds
(echo "%{c}$@"; sleep ${duration}) \
    | lemonbar -g "${barw}x${barh}+${barx}+${bary}" -f "$bar_font" -B "$light_black" -F "$foreground" -d
