#!/bin/bash
killall lemonbar

#define colors
source "$HOME/dotfiles/colors/colors.sh"

#define fonts
# font="Sans-8"
font="-xos4-terminesspowerline-medium-r-normal--12-120-72-72-c-60-iso10646-1"
icon_font="FontAwesome-8"

# Define the clock
clock() {
    DATETIME=$(date "+%H:%M")
    echo -n "$DATETIME"
}

battery() {
    BATPERC=$($HOME/dotfiles/scripts/battery.sh)
    echo -n "$BATPERC"
}

volume() {
    VOL=$($HOME/dotfiles/scripts/volume.sh)
    echo -n "$VOL"
}

#MPD
music() {
    CURSONG=$(mpc current)
    if [ -z $CURSONG ]
    then
	echo -n ""
    else
	echo -n "\uf001 $CURSONG "
    fi
}

ws() {
    local line
    # output="%{F$white} Tags: "
    output=""
    while read line; do
	num=$(echo "${line}" | cut -d , -f 2 | cut -c 5-7)
	if [[ "$line" == *'true'* ]]; then
	    output="${output}%{B#444444}%{F$white}%{U$cyan} ${num} %{U-}"
	elif [[ "$line" == *'false'* ]]; then
	    output="${output}%{F$white}%{B$background}%{U$background} ${num} "
	fi
    done <<< "$(i3-msg -t get_workspaces | jq -S -M -c -r '.[] | [.focused, .name]')"
    echo -e "%{+u}${output}%{F-}%{B-}%{-u}"
}

keyboard() {
    LANG=$(xset -q | gawk 'BEGIN {a[1]="Ru"; a[0]="En"} /LED/ {print a[substr($10,5,1)];}')
    echo -n "$LANG"
}

video_temp() {
    NVIDIA=$(nvidia-smi -q -d TEMPERATURE | sed -n '/GPU Current/p' | gawk '{print $5}')
    echo -n "%{F$light_black} ${sep_left}%{F$white}%{B$light_black} NVIDIA: $NVIDIA"
}

mail_check() {
    MAIL=$($HOME/dotfiles/scripts/mail_checker.py)
    if [ -z $MAIL ]
    then
	echo
    else
	echo -n "%{F$red} ${sep_left}%{F$black}%{B$red} $MAIL%{F-}%{B-}"
    fi
}

# panel geometry
if (xrandr | grep "HDMI-1-0 connected" &>/dev/null); then
    px=1866
else
    px=500
fi

pw=500
ph=20
py=10

# Finally, print bar
while true; do
    if (xrandr | grep "HDMI-1-0 connected" &>/dev/null); then
	echo -e "%{Sf}%{l}$(ws) %{Sl}%{l}$(ws)%{F-}%{B-} %{Sl}%{r} $(mail_check) $(music) $(battery) $(volume)  $(keyboard)  $(clock) "
    else
	echo -e "%{l}$(ws)%{F-}%{B-} %{r} $(mail_check) $(music) $(battery) $(volume)  $(keyboard)  $(clock) "
    fi
    sleep 1
# done | lemonbar -g "${pw}x${ph}+${px}+${py}" -f "$font" -f "$icon_font" -B "$background" -F "$foreground"
done | lemonbar -f "$font" -f "$icon_font" -B "$background" -F "$foreground" -u 2
