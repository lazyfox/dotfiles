#!/bin/bash

background=`xrdb -query | grep "background:" | tail -c 8`
foreground=`xrdb -query | grep "foreground:" | tail -c 8`

black=`xrdb -query | grep "color0:" | tail -c 8`
light_black=`xrdb -query | grep "color8:" | tail -c 8`

red=`xrdb -query | grep "color1:" | tail -c 8`
light_red=`xrdb -query | grep "color9:" | tail -c 8`

green=`xrdb -query | grep "color2:" | tail -c 8`
light_green=`xrdb -query | grep "color10:" | tail -c 8`

yellow=`xrdb -query | grep "color3:" | tail -c 8`
light_yellow=`xrdb -query | grep "color11:" | tail -c 8`

blue=`xrdb -query | grep "color4:" | tail -c 8`
light_blue=`xrdb -query | grep "color12:" | tail -c 8`

magenta=`xrdb -query | grep "color5:" | tail -c 8`
light_magenta=`xrdb -query | grep "color13:" | tail -c 8`

cyan=`xrdb -query | grep "color6:" | tail -c 8`
light_cyan=`xrdb -query | grep "color14:" | tail -c 8`

cyan=`xrdb -query | grep "color6:" | tail -c 8`
light_cyan=`xrdb -query | grep "color14:" | tail -c 8`

white=`xrdb -query | grep "color7:" | tail -c 8`
light_white=`xrdb -query | grep "color15:" | tail -c 8`
