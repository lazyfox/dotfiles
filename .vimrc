"###############################################
"#                                             #
"#              ~/.vimrc                       #
"#           VIM config file                   #
"#                                             #
"###############################################
" vimconf is not vi-compatible
set nocompatible
" Automatically make needed files and folders on first run
call system("mkdir -p $HOME/.vim/{swap,undo}")
""" Vundle plugin manager {{{
    """ Automatically setting up Vundle, taken from
    """ http://www.erikzaadi.com/2012/03/19/auto-installing-vundle-from-your-vimrc/ {{{
        let has_vundle=1
        if !filereadable($HOME."/.vim/bundle/Vundle.vim/README.md")
          echo "Installing Vundle..."
          echo ""
          silent !mkdir -p $HOME/.vim/bundle
          silent !git clone https://github.com/gmarik/Vundle.vim $HOME/.vim/bundle/Vundle.vim
          let has_vundle=0
        endif
    """ }}}
    """ Initialize Vundle {{{
        filetype off                                " required to init
        set rtp+=$HOME/.vim/bundle/Vundle.vim       " include vundle
        call vundle#begin()                         " init vundle
    """ }}}
    """ Install plugins from GitHub {{{
        " Vundle
        Plugin 'gmarik/Vundle.vim'
        " Edit files using sudo/su
        Plugin 'chrisbra/SudoEdit.vim'
        " Glorious colorscheme
        Plugin 'nanotech/jellybeans.vim'
        " Super easy commenting, toggle comments etc
        Plugin 'scrooloose/nerdcommenter'
        " Highlighting
        Plugin 'PotatoesMaster/i3-vim-syntax'
        Plugin 'smancill/conky-syntax.vim'
        " NERD Tree
        Plugin 'scrooloose/nerdtree'
        " Vim-airline statusbar
        Plugin 'bling/vim-airline'
        " RusMod
        Plugin 'vim-scripts/ruscmd'
        " Autocomplit by <TAB>
        Plugin 'ervandew/supertab'
        " Git wrapper inside Vim
        Plugin 'tpope/vim-fugitive'
        " Displays the changes for a commits
        Plugin 'airblade/vim-gitgutter'
        " Full path fuzzy file finder for Vim.
        Plugin 'kien/ctrlp.vim'
    """ }}}
    " Finish Vundle stuff
    call vundle#end()
    """ Installing plugins the first time, quits when done  {{{
        if has_vundle == 0
            :silent! PluginInstall
            :qa
        endif
    """ }}}
""" }}}
""" User's setting {{{
    syntax on                                   " syntax highlighting
    colorscheme jellybeans                      " colorscheme from plugin
    set cursorline                              " hilight cursor line
    filetype plugin on                          " need for 'nerdcommenter'
    set number                                  " line numbers
    set scrolloff=3                             " lines above/below cursor
    set wildmenu                                " improved auto complete
    set wildignore=*.a,*.o,*.so,*.pyc,*.jpg,
                \*.jpeg,*.png,*.gif,*.pdf,*.git,
                \*.gif,*.jpg,*.JPG,*.xcf,*.JPEG,
                \*.tgz,*.zip,*.rar,
                \*.swp,*.swo                    " tab completion ignores
    set hlsearch                                " highlight search
    """ <Tab> settings {{{
        " http://habrahabr.ru/post/64224/
        " https://github.com/timss/vimconf/blob/master/.vimrc
        set expandtab           " replace tab characters with spaces
        set tabstop=4           " replace <TAB> w/4 spaces
        set shiftwidth=4        " default 8
        set smarttab            " the behavior tab in the beginning of the line
        set softtabstop=4       " "tab" feels like <tab>
    """ }}}
    """ Force behavior and filetypes, and by extension highlighting {{{
        augroup FileTypeRules
            autocmd!
            autocmd BufNewFile,BufRead *.md set ft=markdown tw=79
            autocmd BufNewFile,BufRead *.tex set ft=tex tw=79
            autocmd BufNewFile,BufRead *.lua set ft=lua tw=79
            autocmd BufNewFile,BufRead *.txt set ft=sh tw=0
            autocmd BufNewFile,BufRead *.sh set ft=sh tw=79
            autocmd BufNewFile,BufRead *.cpp set tw=79
            autocmd BufNewFile,BufRead *.c set tw=79
            autocmd BufNewFile,BufRead *.py set tw=79
        augroup END
    """ }}}
    """ Custom highlighting, where NONE uses terminal background {{{
        function! CustomHighlighting()
            highlight Normal ctermbg=NONE
            highlight NonText ctermbg=NONE
            highlight LineNr ctermbg=NONE
            highlight SignColumn ctermbg=NONE
            highlight SignColumn guibg=#151515
            highlight CursorLine ctermbg=235
        endfunction

        call CustomHighlighting()
    """ }}}
    """ Gvim {{{
        if has("gui_running")
            set guifont=Liberation\ Mono\ for\ Powerline\ 9
            set guioptions-=m       " remove menubar
            set guioptions-=T       " remove toolbar
            set guioptions-=r       " remove right scrollbar
            set guioptions-=R       " remove right scrollbar
            set guioptions-=l       " remove left scrollbar
            set guioptions-=L       " remove left scrollbar
            set guioptions-=b       " remove bottom (horizontal) scrollbar
        endif
    """ }}}
    """ Highlight unwanted spaces {{{
        highlight ExtraWhitespace ctermbg=red guibg=red
        autocmd ColorScheme * highlight ExtraWhitespace ctermbg=red guibg=red
        match ExtraWhitespace /\s\+\%#\@<!$/
        autocmd InsertLeave * redraw!
    """ }}}
""" }}}
""" Plugins settings {{{
    """ Settings for Vim-Airline {{{
        set laststatus=2
        let g:airline_theme='bubblegum'
        let g:airline_powerline_fonts = 1
        let g:airline#extensions#tabline#enabled = 1
        let g:airline#extensions#tabline#formatter = 'unique_tail'
    """ }}}
    """ NERDTree SETTINGS {{{
        let NERDTreeShowLineNumbers=0
        let NERDTreeShowHidden=1
        let g:Tlist_Show_One_File = 1
        let NERDTreeQuitOnOpen=1
        let NERDTreeIgnore = [
                    \'\.DS_Store$',
                    \'\.bundle$','\.git$'
                    \]
        let NERDTreeShowBookmarks=1
        let NERDTreeBookmarksFile=expand("$HOME/.vim/.NERDTreeBookmarks")
        let g:NERDTreeWinSize=35
        let NERDTreeChDirMode=2
    """ }}}
    """ Settings for CtrlP {{{
        let g:ctrlp_clear_cache_on_exit = 0
        let g:ctrlp_working_path_mode = ''
        let g:ctrlp_show_hidden = 1
        let g:ctrlp_custom_ignore = {
            \ 'dir':  '\v[\/]\.(git|hg|svn|tmp)$',
            \ }
        " Don't split in Startify
        let g:ctrlp_reuse_window = 'startify'
    """ }}}
""" }}}
""" Keybindings {{{
    " Remap <leader>
    let mapleader=","
    " Quickly edit/source .vimrc
    noremap <leader>ve :edit $HOME/dotfiles/.vimrc<CR>
    noremap <leader>vs :source $HOME/dotfiles/.vimrc<CR>
    " Yank(copy) to system clipboard
    noremap <leader>y "+y
    " Paste from clipboard
    noremap <leader>p "+p
    " Treat wrapped lines as normal lines
    nnoremap j gj
    nnoremap k gk
    " Open/close NERDTree window
    noremap <F4> :NERDTreeToggle<CR>
    " Buffers, preferred over tabs now with bufferline.
    nnoremap gn :bnext<CR>
    nnoremap gN :bprevious<CR>
    nnoremap gd :bdelete<CR>
    " previous open buffer
    nnoremap gf <C-^>
    " Press F8 to toggle highlighting on/off, and show current value.
    noremap <F8> :set hlsearch! hlsearch?<CR>
    " Update plugins
    nnoremap <leader>bi :BundleInstall!<CR>
    " List of installed plugins
    nnoremap <leader>bl :BundleList<CR>
    " Figitive keybinding
    noremap <leader>ga :Gwrite<CR>
    noremap <leader>gs :Gstatus<CR>
    noremap <leader>gp :Gpush<CR>
    noremap <leader>gc :Gcommit<CR>
"" }}}
