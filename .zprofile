#################################################################
#                                                               #
#   .zprofile file                                              #
#                                                               #
#   Executed from the Z shell when you log in.                  #
#                                                               #
#################################################################

if [ -f "$HOME/.zshrc" ] ; then
        source $HOME/.zshrc
fi


# variables
if [ -d $HOME/bin ] ; then
    export PATH=$HOME/bin:$PATH
fi

export GREP_COLOR='01;33'
export LIBVA_DRIVER_NAME=vdpau
export MPLAYER_HOME=$HOME/.config/mplayer
export EDITOR=`which nano`
export I3_FOLDER=$HOME/.config/i3

export MB_WARBAND="$HOME/.local/share/Steam/steamapps/common/MountBlade Warband"
export TORRENTS="$HOME/.local/share/rtorrent/torrents"

export XDG_MENU_PREFIX=gnome-applications.menu
export XDG_CONFIG_HOME="$HOME/.config"
export PAGER=less
export DEFAULT_USER="lazyfox"

# need for Steam
#export LD_PRELOAD="/usr/lib32/libasound.so.2:/usr/lib/libasound.so.2"
#export STEAM_RUNTIME=0

# MPD daemon start (if no other user instance exists)
# [ ! -s ~/.config/mpd/pid ] && mpd
