#################################################################
#                                                               #
#   .bash_profile file                                          #
#                                                               #
#   Executed from the bash shell when you log in.               #
#                                                               #
#################################################################

if [ -f "$HOME/.bashrc" ] ; then
        source $HOME/.bashrc
fi


# variables
if [ -d $HOME/bin ] ; then
    export PATH=$HOME/bin:/usr/local/bin:$PATH
fi

export MB_WARBAND="$HOME/.local/share/Steam/steamapps/common/MountBlade Warband"
export TORRENTS="$HOME/.local/share/rtorrent/torrents"
export GREP_COLOR='01;33'
export LIBVA_DRIVER_NAME=vdpau
